﻿using JavaOpenMacroInput;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapMapCommandToJavaOMI : MonoBehaviour
{
    public UIMapLeftRightToInputField m_tapDetected;
    public ExperimentJavaOMIConverter m_converter;
    void OnEnable()
    {
        m_tapDetected.m_onTapReceivedTranslated+=TapToOmi;


    }
    void OnDisable()
    {
        m_tapDetected.m_onTapReceivedTranslated-=TapToOmi;


    }

    private void TapToOmi(TapReceived received, string translation)
    {
        string []cmds =  m_converter.TryToParse(translation);
        foreach (JavaOMI item in JavaOMI.GetAllRunningRegistered())
        {
            if (item != null) {
                if (cmds.Length <= 0)
                {
                    item.PastText(translation);
                }
                else {
                    for (int i = 0; i < cmds.Length; i++)
                    {
                        item.SendRawCommand(cmds[i]);
                    }
                }
            }
        }
        
    }
}
