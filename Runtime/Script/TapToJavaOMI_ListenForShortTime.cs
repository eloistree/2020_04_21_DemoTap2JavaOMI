﻿using JavaOpenMacroInput;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TapToJavaOMI_ListenForShortTime : MonoBehaviour
{
    public Toggle m_laserMouse;
    public Toggle m_airMouse;
    public bool m_pauseSwitch;

    public bool m_isInListenMode;
    public UnityEvent m_startListening;
    public UnityEvent m_stopListening;
    public float m_timeListened=2.5f;
    public float m_cooldown;
    public void SetTimeToListent(float valueInSecond)
    {

        m_timeListened = Mathf.Clamp( valueInSecond,0.1f,10);
    }
    public void SetAsPause(bool value)
    {
        m_pauseSwitch = value;
    }
    public DateTime lastTimeCheck;
    public DateTime now;
    void Update()
    {
        if (m_pauseSwitch)
            return;
   

         now = DateTime.Now;
            ConnectedTap left = TapUtility.GetLeft();
            ConnectedTap right = TapUtility.GetRight();
            if ((left!=null && left.GetMouseInfo().GetDelta(lastTimeCheck, now).Length > 0) || (right!=null &&right.GetMouseInfo().GetDelta(lastTimeCheck, now).Length > 0))
            {
                m_cooldown = m_timeListened;
              
                    if (!m_isInListenMode) {
                          m_isInListenMode = true;
                          m_startListening.Invoke();
                    }
           
                
            }
        float previousCooldown = m_cooldown;
        m_cooldown -= Time.deltaTime;
        if (previousCooldown > 0f && m_cooldown <= 0f) {
            m_isInListenMode = false;
            m_stopListening.Invoke();
        }


        lastTimeCheck = now;
    
    }
}
