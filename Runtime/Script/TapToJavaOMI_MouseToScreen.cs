﻿using JavaOpenMacroInput;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TapToJavaOMI_MouseToScreen : MonoBehaviour
{
    public UI_ServerDropdownJavaOMI m_serverSelected;
    public UI_FakeCursorScreen m_fakeScreenFromTap;
    public bool m_pauseSendOfData;
    public SendType m_sendType;
    public enum SendType{ Pourcent, Pixel }
    public InputField m_handX;
    public InputField m_handY;
    public float m_sendDelay = 0.1f;


    public void SetDelay(float value) {

        m_sendDelay = value;
        CancelInvoke("SendMouseData");
        InvokeRepeating("SendMouseData", 0, m_sendDelay);
    }

    public void SetAsPause(bool value) {
        m_pauseSendOfData = value;
    }

    void Awake() {
        InvokeRepeating("SendMouseData", 0, m_sendDelay);

    }

    public Vector2 lastPos = new Vector2();
   
    void SendMouseData()
    {
        Vector2 pos = m_fakeScreenFromTap.GetCursorPixelPosition();
        m_handX.text = "" + pos.x;
        m_handY.text = "" + pos.y;
        if (m_pauseSendOfData)
            return;

        if (pos == lastPos)
            return;
        
        List<JavaOMI> devices = m_serverSelected.GetJavaOMISelected();
        for (int i = 0; i < devices.Count; i++)
        {
            JavaOMI deviceTarget = devices[i];
            //if (deviceTarget != null)
            deviceTarget.MouseMove((int)-pos.x, (int)-pos.y);

        }
        lastPos = pos;
    }


}
