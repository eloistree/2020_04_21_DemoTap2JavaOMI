﻿using JavaOpenMacroInput;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TapToJavaOMI_FingersToKeyLeftAndRight : MonoBehaviour
{
    public UI_ServerDropdownJavaOMI m_serverSelected;
    public Dropdown m_typeOfKeySend;
    public bool m_pauseSendOfData;
    public SendType m_keyType;
    public enum SendType{ Numpad_0_9, F_1_10,  K_1_9, Custom}


   

    private void Awake()
    {
        m_typeOfKeySend.onValueChanged.AddListener(SetKeySend);
    }

    public void SetAsPause(bool value) {
        m_pauseSendOfData = value;
    }
    public void SetKeySend(int value) {

        switch (value)
        {
            case 0:
                m_keyType = SendType.Numpad_0_9; return;
            case 1:
                m_keyType = SendType.F_1_10; return;
            case 2:
                m_keyType = SendType.K_1_9; return;
   default:
                break;
        }
    }

    public DateTime lastTimeCheck;
    public DateTime now;
    void Update()
    {

        if (m_pauseSendOfData)
            return;
        now = DateTime.Now;
        ConnectedTap left = TapUtility.GetLeft();
        ConnectedTap right = TapUtility.GetRight();
        TapReceived lastTap;
        TapReceived[] foundTaps;

        List<JavaOMI> devices = m_serverSelected.GetJavaOMISelected();
        Debug.Log("left "+left +"  " +devices.Count);
        for (int i = 0; i < devices.Count; i++)
        {
            JavaOMI deviceTarget = devices[i];
            //if (deviceTarget != null) 
            { 
            if (left != null)
            {
                if (left.WasTapSince(lastTimeCheck, now, out lastTap, out foundTaps))
                {


                    if (lastTap.IsOn(Finger.Pinky))
                        foreach (var item in GetKey(HandSide.Left, Finger.Pinky, m_keyType))
                            deviceTarget.Keyboard(item);
                    if (lastTap.IsOn(Finger.Ring))
                        foreach (var item in GetKey(HandSide.Left, Finger.Ring, m_keyType))
                            deviceTarget.Keyboard(item);
                    if (lastTap.IsOn(Finger.Middle))
                        foreach (var item in GetKey(HandSide.Left, Finger.Middle, m_keyType))
                            deviceTarget.Keyboard(item);
                    if (lastTap.IsOn(Finger.Index))
                        foreach (var item in GetKey(HandSide.Left, Finger.Index, m_keyType))
                            deviceTarget.Keyboard(item);
                    if (lastTap.IsOn(Finger.Thumb))
                        foreach (var item in GetKey(HandSide.Left, Finger.Thumb, m_keyType))
                            deviceTarget.Keyboard(item);
                }
            }
            if (right != null)
            {

                if (right.WasTapSince(lastTimeCheck, now, out lastTap, out foundTaps))
                {


                    if (lastTap.IsOn(Finger.Pinky))
                        foreach (var item in GetKey(HandSide.Right, Finger.Pinky, m_keyType))
                            deviceTarget.Keyboard(item);
                    if (lastTap.IsOn(Finger.Ring))
                        foreach (var item in GetKey(HandSide.Right, Finger.Ring, m_keyType))
                            deviceTarget.Keyboard(item);
                    if (lastTap.IsOn(Finger.Middle))
                        foreach (var item in GetKey(HandSide.Right, Finger.Middle, m_keyType))
                            deviceTarget.Keyboard(item);
                    if (lastTap.IsOn(Finger.Index))
                        foreach (var item in GetKey(HandSide.Right, Finger.Index, m_keyType))
                            deviceTarget.Keyboard(item);
                    if (lastTap.IsOn(Finger.Thumb))
                        foreach (var item in GetKey(HandSide.Right, Finger.Thumb, m_keyType))
                            deviceTarget.Keyboard(item);
                }
            }
            }
        }
        lastTimeCheck = now;
    }


    public List<JavaKeyEvent> GetKey(HandSide hand, Finger finger, SendType sendType) {
        List<JavaKeyEvent> toType = new List<JavaKeyEvent>();
        if (hand == HandSide.Left)
        {
            switch (finger)
            {
                case Finger.Pinky :
                    if (sendType == SendType.Numpad_0_9) toType.Add(JavaKeyEvent.VK_NUMPAD0);
                    else if (sendType == SendType.F_1_10) toType.Add(JavaKeyEvent.VK_F1);
                    else if (sendType == SendType.K_1_9) toType.Add(JavaKeyEvent.VK_1);
                    break;
                case Finger.Ring :
                    if (sendType == SendType.Numpad_0_9) toType.Add(JavaKeyEvent.VK_NUMPAD1);
                    else if (sendType == SendType.F_1_10) toType.Add(JavaKeyEvent.VK_F2);
                    else if (sendType == SendType.K_1_9) toType.Add(JavaKeyEvent.VK_2);
                    break;
                case Finger.Middle:
                    if (sendType == SendType.Numpad_0_9) toType.Add(JavaKeyEvent.VK_NUMPAD2);
                    else if (sendType == SendType.F_1_10) toType.Add(JavaKeyEvent.VK_F3);
                    else if (sendType == SendType.K_1_9) toType.Add(JavaKeyEvent.VK_3);
                    break;
                case Finger.Index:
                    if (sendType == SendType.Numpad_0_9) toType.Add(JavaKeyEvent.VK_NUMPAD3);
                    else if (sendType == SendType.F_1_10) toType.Add(JavaKeyEvent.VK_F4);
                    else if (sendType == SendType.K_1_9) toType.Add(JavaKeyEvent.VK_4);
                    break;
                case Finger.Thumb:
                    if (sendType == SendType.Numpad_0_9) toType.Add(JavaKeyEvent.VK_NUMPAD4);
                    else if (sendType == SendType.F_1_10) toType.Add(JavaKeyEvent.VK_F5);
                    else if (sendType == SendType.K_1_9) toType.Add(JavaKeyEvent.VK_5);
                    break;
                default:
                    break;
            }

        }
        if (hand == HandSide.Right)
        {
            switch (finger)
            {
                case Finger.Thumb:
                    if (sendType == SendType.Numpad_0_9) toType.Add(JavaKeyEvent.VK_NUMPAD5);
                    else if (sendType == SendType.F_1_10) toType.Add(JavaKeyEvent.VK_F6);
                    else if (sendType == SendType.K_1_9) toType.Add(JavaKeyEvent.VK_6);
                    break;
                case Finger.Index:
                    if (sendType == SendType.Numpad_0_9) toType.Add(JavaKeyEvent.VK_NUMPAD6);
                    else if (sendType == SendType.F_1_10) toType.Add(JavaKeyEvent.VK_F7);
                    else if (sendType == SendType.K_1_9) toType.Add(JavaKeyEvent.VK_7);
                    break;
                case Finger.Middle:
                    if (sendType == SendType.Numpad_0_9) toType.Add(JavaKeyEvent.VK_NUMPAD7);
                    else if (sendType == SendType.F_1_10) toType.Add(JavaKeyEvent.VK_F8);
                    else if (sendType == SendType.K_1_9) toType.Add(JavaKeyEvent.VK_8);
                    break;
                case Finger.Ring:
                    if (sendType == SendType.Numpad_0_9) toType.Add(JavaKeyEvent.VK_NUMPAD8);
                    else if (sendType == SendType.F_1_10) toType.Add(JavaKeyEvent.VK_F9);
                    else if (sendType == SendType.K_1_9) toType.Add(JavaKeyEvent.VK_9);
                    break;
                case Finger.Pinky:
                    if (sendType == SendType.Numpad_0_9) toType.Add(JavaKeyEvent.VK_NUMPAD9);
                    else if (sendType == SendType.F_1_10) toType.Add(JavaKeyEvent.VK_F10);
                    else if (sendType == SendType.K_1_9) toType.Add(JavaKeyEvent.VK_0);
                    break;
                default:
                    break;
            }

        }
        return toType;
    }
}
